package net.za.chat.bsmodal;

import javax.lang.model.element.Name;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;

/**
 * Created by Mike on 22/08/2015.
 */
public class TestServlet extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest req,HttpServletResponse resp) throws ServletException {

    }

    @Override
    public void doPost(HttpServletRequest req,HttpServletResponse resp) throws ServletException,IOException {
            Writer out = resp.getWriter();
            out.append("<H1>Result</H1><TABLE><TR><TH>ID</TH><TH>NAME</TH></TR>");
                for (Application a: Application.values()) {
                    out.append("<TR><TD><A onclick=\"javascript:setApp('"+a.name+"');\" class=\"mylink\">"+a.name()+"</A></TD><TD>"+a.getName()+"</TD></TR>");
                }
            out.append("</TABLE>");

    }

        public enum Application {
            ONE ("Wun"),
            TWO ("Too"),
            THREE ("Three");

            private String name;
            Application(String name) {
                this.name=name;
            }

            public String getName() {
                return name;
            }
        }
}
